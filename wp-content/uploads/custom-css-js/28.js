<!-- start Simple Custom CSS and JS -->
<script type="text/javascript">
jQuery(document).ready(function() {
   
    var url_flecha = "https://i.ibb.co/2YTjL5F/linea.png";
    
	
	
	var datos_otros_intereses={
       "data": [
           {
               "titulo": "Sabes que son los datos abiertos?",
               "descripcion": "La información que producen las entidades públicas a tu alcance",
			   "imagen": "https://i.ibb.co/G91kXTX/cuadro-1.png",
               "enlace": "Me interesa"
           },
           {
               "titulo": "Conoce más sobre nuestro país",
               "descripcion": "Colombia es hermosa, y queremos que conozcas más sobre ella",
			   "imagen": "https://i.ibb.co/hMsZRbP/cuadro-2.png",
			   "enlace": "Ir a Colombia.co"
           },
           {
               "titulo": "Este portal está pensado para ti",
               "descripcion": "GOV.CO nace para facilitarle a los ciudadanos la interacción con el Estado",
			   "imagen": "https://i.ibb.co/q9sWjwz/cuadro-3.png",
                "enlace": "Quiero saber como funciona"
           },
		   {
               "titulo": "Destacado que esté en acondicionado",
               "descripcion": "Texto descriptivo que capte la atención del lector",
			   "imagen": "https://i.ibb.co/VNy8k6b/cuadro-4.png",
               "enlace": "Call to action"
           },
       ]
     }
    
	var data_parsiada = JSON.stringify(datos_otros_intereses.data);
	var data_final    = JSON.parse(data_parsiada);
    var titulo_seccion_otros_temas = "Otros temas de tu interés";
	var cadena = '<div class="col-md-12"><h2 class="tarjetas_titulo">'+titulo_seccion_otros_temas+'</h2></div>';
	
	jQuery.each(data_final, function( index, value ) {
		cadena += '<div class="col-md-6 container_tarjeta"><div class="tarjeta"><img class="img_principal" src="'+value.imagen+'" /><h2>'+value.titulo+'</h2><p>'+value.descripcion+'</p><spam>'+value.enlace+'<img src="'+url_flecha+'"/></spam></div></div>';
	   
	});
	 jQuery("#seccion_otros_temas_interes").append(cadena);  
	 
	 
	 
	 
	
	var datos_quremos_conocer_tu_opinion ={
       "data": [
           {
			   "aviso": "Falta un día",
               "titulo": "Únete al Pacto por Colombia!",
               "descripcion": "Presidencia de la República",
			   "descripcion_2": "534 colombianos participando"
           },
           {
			   "aviso": "Activo!",
               "titulo": "¿Cómo mejorarías nuestro sistema de transporte?",
               "descripcion": "Secretaría de Movilidad de Bogotá",
			   "descripcion_2": "87 colombianos participando"
           },
           {
			   "aviso": "Conoce los resultados",
               "titulo": "Los datos y visualizaciones del gobierno interesantes para su uso, aprovechamiento y toma de decisiones.",
               "descripcion": "Los datos y visualizaciones del gobierno interesantes para su uso, aprovechamiento y toma de decisiones.",
			   "descripcion_2": ""
           },
       ]
     }
	 
	 var datos_quremos_conocer_tu_opinion_comentarios ={
               "titulo": "CUÉNTANOS",
               "descripcion": "¿Cómo podemos mejorar GOV.CO?",
			   "descripcion_2": "Estamos construyendo este espacio para ti y tu opinión nos ayuda a crecer.",
			   "mensaje_guia": "Escribe aquí tu opinión.",
			   "maximo_caracteres": "255",
			   "boton_envio": "Enviar mi opinión",
          
     }
	 
	 
           
    
	var data_parsiada = JSON.stringify(datos_quremos_conocer_tu_opinion.data);
	var data_final    = JSON.parse(data_parsiada);
    var color  = ['color_naranja','color_verde','color_azul'];
    var titulo_seccion_queremos_tu_opinion = "Queremos conocer tu opinión";
	
    // titulo
	var seccion_izquierda = '<h2 class="tarjetas_titulo">'+titulo_seccion_queremos_tu_opinion+'</h2> ';
	
	
	///
	jQuery.each(data_final, function( index, value ) {
		seccion_izquierda += '<p class="'+color[index]+'">'+value.aviso+'</p><p class="descripcion_1_color">'+value.titulo+'</p><spam class="descripcion_2_color">'+value.descripcion+'</spam><spam class="descripcion_3_color">'+value.descripcion_2+'</spam><hr>';
	   
	});
     seccion_izquierda +='<span class="more">Conocer otros ejercicios de participación <img src="'+url_flecha+'"/></span>';
	 
	 jQuery(".opinión_lista").append(seccion_izquierda); 


    /// 
	var seccion_derecha = '<p id="cuentanos">'+datos_quremos_conocer_tu_opinion_comentarios.titulo+'</p><p id="mejorar">'+datos_quremos_conocer_tu_opinion_comentarios.descripcion+'</p><p id="construyendo_p">'+datos_quremos_conocer_tu_opinion_comentarios.descripcion_2+'</p><div id="mensaje"> <p id="mensaje_escribir">'+datos_quremos_conocer_tu_opinion_comentarios.mensaje_guia+'</p><hr id="linea_mensaje"><span id="caracteres">'+datos_quremos_conocer_tu_opinion_comentarios.maximo_caracteres+'</span></div><div id="div_btn_envio"><button id="btn_envio">'+datos_quremos_conocer_tu_opinion_comentarios.boton_envio+'</button></div>';
	jQuery(".cuadro").append(seccion_derecha);	 
	 
	 
	
	 
});</script>
<!-- end Simple Custom CSS and JS -->
